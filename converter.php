<?php
/**
 * @package currency converter
 * Intermediate Script
 * @uses currencyConverter.class
 */
error_reporting(E_ALL);
include_once('currencyConverter.class.php');

$converterObj = new currencyConverter();
//$converterObj->ErrorLog = FALSE; //To Turn off the Exception Logging [Not Recommended].
//$converterObj->OpenExchangeAppId = ''; //Open Exchange App ID. Please see README for details.
$converterObj->CurrencyApiKey = '1cd79c61dd1b6b50f7eb2026e397e75e49c82221'; //Currency API Key. Please see README for more details.

$converterObj->FromCurrency = $_POST['from'];
$converterObj->ToCurrency = $_POST['to'];
$amount = $_POST['amt'];
//$amount = 5;

echo $amount." ".$_POST['from']. " is ".$converterObj->convert($amount)." ".$_POST['to'];
exit;