# README #
Currency Converter is a multi-API based realtime money converter. We cannot guarantee the accuracy of the conversion as it is based on several feeds out there. Currently we are using three currency feeds available freely on the net.

### What is this repository for? ###

* We are using OpenExchange, Yahoo Finance and Rate Exchange.
* Version: 0.2.1
* [Real-time Currency Converter PHP Class](http://thecoderin.com/php/real-time-currency-converter-php-class)

### Using the class ###

* Download the package from PHP Classes or from BitBucket repo.
* Open Exchange Account with AppId - To enable the Open Exchange feed, we need an AppId which we can generate from https://openexchangerates.org by clicking the Get Your App Id Link on the top right of the site. Under the plans you can see the "Forever Free" plan and register with your email to get the AppId.
+AppId for testing purpose: dde15ce68f0a42709c53c6e286d6194c [1000 Requests/Month]

*  Currency API with Key - To enable Currency API, we need to login to the site http://currency-api.appspot.com/. On login to the site, you will get an API Key generated in your Dashboard. This API only support limited currencies, but provide reliable fast service.
+Key for testing purpose: 1cd79c61dd1b6b50f7eb2026e397e75e49c82221 [3000 Request/Month]

* The Other Feed - We have Yahoo Finance(http://finance.yahoo.com) doesn't need any Key or Registration to access the feeds and get the result.

* Why Three Feeds - I use three feeds, all having most accurate feeds with 99% uptime guaranteed. Two of the feeds need Registration and to get App Key. The third one (Yahoo), does not need any Key to get the data. So at least one of the Conversion works always, which gives 100% result availability.

* Exceptions.log - Make sure the exceptions.log has full permission, so that the class can write the issues it gets on converting feeds. It is always recommended to turn on the Exceptions to identify the issues. You can turn the Error Log off by changing the value of the ErrorLog variable to false.


### Who do I talk to? ###

* Author: Anish Karim
* Email: thecoderinATgmailDOTcom
* Web: http://thecoderin.com