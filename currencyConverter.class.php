<?php
/**
 * @category money conversion
 * @package currency converter
 * @uses currency converter APIs
 * @author Anish Karim <thecoderin@gmail.com>
 * @license http://opensource.org/licenses/GPL-3.0 GPL
 */
class currencyConverter {
  
  public $FromCurrency;
  public $ToCurrency;
  public $ErrorLog;
  
  public $OpenExchangeBase;
  public $OpenExchangeAppId;
  
  public $Yahoo;
  
  public $CurrencyApiKey;
  public $CurrencyApiURL;
  
  /**
   * Function Construct
   */
  
  function __construct() {
    $this->FromCurrency = 'USD';
    $this->ToCurrency = 'INR';
    $this->ErrorLog = TRUE;
    
    $this->OpenExchangeBase = 'http://openexchangerates.org/api/latest.json';
    
    $this->CurrencyApiURL = "http://currency-api.appspot.com/api";
    
    $this->Yahoo = "http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s=";
  }
  /**
   * Function Convert
   * scope Public
   * @param float $amount Amount to be converted
   * @return float $convertedAmount
   */
  public function convert($amount) {
    $rate = 0.00;
    $convertedAmount = FALSE;
    if ($amount > 0) {
      if ($rate = $this->openExchange());
      elseif ($rate = $this->currencyAPI());
      else ($rate = $this->yahooFinance());
      $convertedAmount = $this->calculateMoney($rate, $amount);
    }
    return $convertedAmount;
  }
  /**
   * Function Calculate Money
   * scope Protected
   * @param float $rate Currency Rate as per the data from API.
   * @param float $amount The amount of currency which needs to Convert.
   * @return float $convertedAmount The amount converted as per the conversion value.
   */
  protected function calculateMoney($rate=0.00, $amount=1.00) {
    $convertedAmount = $rate * $amount;
    return sprintf("%.2f", $convertedAmount);
  }
  /**
   * Function Currency API
   * scope Protected
   * @param None
   * @return float $rate the Currency Rate
   * @todo This function is not tested as the site is not available.
   */
  protected function currencyAPI(){
    if (!$this->CurrencyApiKey) return false;
    $URL = $this->CurrencyApiURL . '/' . $this->FromCurrency . '/' . $this->ToCurrency . '.json?key=' . $this->CurrencyApiKey;
    $json = $this->_getJSON($URL);
    if ($json) {
      return $json->rate;
    }
    return false;
  }
  /**
   * Function Open Exchange
   * scope Protected
   * @param None
   * @return float $rate the Currency Rate
   * @todo Needs correction on the function, we are getting a difference of +0.13 on 1 EUR to INR, while comparing with Yahoo Finance Feed.
   */
  protected function openExchange(){
    if (!$this->OpenExchangeAppId) return false;
    
    $URL = $this->OpenExchangeBase.'?app_id='.$this->OpenExchangeAppId;
    $json = $this->_getJSON($URL);
    $base = $json->base;
    $currencyRates = $json->rates;
    $from = $this->FromCurrency;
    $to = $this->ToCurrency;
    $base2from = floor($currencyRates->$from, 2);
    $to2base = floor($currencyRates->$to, 2);
    $rate = (round((1/$base2from), 2)) * $to2base;
    return $rate;
  }
  /**
   * Function Yahoo Finance
   * scope Protected
   * @param None
   * @return float $rate conversion rate.
   */ 
  protected function yahooFinance() {
    $URL = $this->Yahoo.$this->FromCurrency.$this->ToCurrency."=X";
    $csv = file_get_contents($URL);
    $data = explode(',', $csv);
    //$this->_test($data);
    return $data[1];
  }
  /**
   * Function Test
   * scope Private
   * @param Array $arr
   * @return NULL
   * For showing the value, testing purpose.
   */
  private function _test($arr) {
    print "<pre>";
    print_r($arr);
    print "</pre>";
  }
  /**
   * Fumction getJSON
   * scope Private
   * @param String $URL
   * @return Object $json
   */ 
  private function _getJSON($URL) {
    $ch = curl_init($URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    curl_close($ch);
    return $this->_jsonValidate($data);
  }
  /**
   * Function Json Validate
   * @param String $json
   * @return Object $jsonObj
   * scope Private
   */
  private function _jsonValidate($json) {
    $jsonObj = json_decode($json);
    switch (json_last_error()) {
      case JSON_ERROR_NONE:
        $error = false;
        break;
      case JSON_ERROR_DEPTH:
        $error = 'Maximum stack depth exceeded.';
        break;
      case JSON_ERROR_STATE_MISMATCH:
        $error = 'Underflow or the modes mismatch.';
        break;
      case JSON_ERROR_CTRL_CHAR:
        $error = 'Unexpected control character found.';
        break;
      case JSON_ERROR_SYNTAX:
        $error = 'Syntax error, malformed JSON.';
        break;
      default:
        $error = 'Unknown JSON error occured.';
        break;
    }
    if (isset($jsonObj->success) && isset($jsonObj->message)) {
      if ($jsonObj->success == false && !$error) {
        $error = $jsonObj->message;
      }
    }
    if ($error) {
      $info = array(
        'json' => $json,
        'error' => $error
      );
      if ($this->ErrorLog) $this->_logException($info);
      return false;
    }
    else return $jsonObj;
    
  }
  /**
   * Function logException
   * @param Array errorInfo
   * @return None.
   * scope Private
   */
  private function _logException($errorInfo) {
    $file = 'exception.log';
    $handler = fopen($file,'a+');
    
    if (is_array($errorInfo)) {
      $content= time() . ">>" . print_r($errorInfo, true);
    } else {
      $content= time() . ">>" . $errorInfo;
    }
    if ($handler) {
      fputs($handler, $content, strlen($content));
      fclose($handler);
    }
    return;
  }
}
?>